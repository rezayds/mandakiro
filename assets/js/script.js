$(function () {
	$(document).scroll(function () {
		var $nav = $(".fixed-top");
		$nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
		$('.nav-link').toggleClass('scrolled', $(this).scrollTop() > $nav.height());
		$('.navbar-toggler').toggleClass('scrolled', $(this).scrollTop() > $nav.height());
		$('.navbar-toggler-icon').toggleClass('scrolled', $(this).scrollTop() > $nav.height());
		if ($(this).scrollTop() > $nav.height()) {
			$('.navbar-brand img').attr('src', "assets/img/icon/mandakiro-white.svg");
		} else {
			$('.navbar-brand img').attr('src', "assets/img/icon/mandakiro.svg");
		}
	});
});